#include <iostream>
#include <math.h>
#include <chrono>

using namespace std;

unsigned long long fibList[300]={0};


unsigned long long fibonnaciRecDin(int n){
	if(n<2)
		return fibList[n];
	if(fibList[n] == 0)
		fibList[n] = fibonnaciRecDin(n-1)+fibonnaciRecDin(n-2);
	return fibList[n];
}

unsigned long long fibonnaciRecursivo(int n){
	if(n<0)
		return -1;
	if(n==0)
		return 0;
	if(n==1)
		return 1;
	return fibonnaciRecursivo(n-1)+fibonnaciRecursivo(n-2);
	
}

unsigned long long fibonnaciLoop(int n){
	unsigned long long f0=0;
	unsigned long long f1=1;
	unsigned long long fn;	
	if(n<0)
		return -1;
	if(n==0)
		return 0;
	if(n==1)
		return 1;
	n = n-1;
	do{
		fn=f0+f1;
		f0=f1;
		f1=fn;
		n=n-1;
	}while(n>0);
	return fn;
}

unsigned long long fibonnaciFormula(int n){
	double phi = pow(((1.0+sqrt(5.0))/2.0), n);
	return round(phi/sqrt(5.0));
}

int main(){
	int n;
	cin >> n;
	fibList[0]=0;
	fibList[1]=1;
	unsigned long long ans;
	std::chrono::system_clock::time_point start;
	std::chrono::system_clock::time_point end;
	// Fibonnaci loop de repeticao	
	/*
	start = chrono::system_clock::now();
	ans = fibonnaciLoop(n);
	end = chrono::system_clock::now();
	cout << ans << '\t' << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count() << '\n';
	// Fibonnaci pela formula
	*/
	start = chrono::system_clock::now();
	ans = fibonnaciFormula(n);
	end = chrono::system_clock::now();
	cout << ans << '\t' << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()<< '\n';
	/*// Fibonnaci recursivo dinamico
	start = chrono::system_clock::now();
	ans=fibonnaciRecDin(n);
	end = chrono::system_clock::now();
	cout << ans << '\t' << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()<< '\n';
	
	// Fibonnaci recursivo
	start = chrono::system_clock::now();
	ans=fibonnaciRecursivo(n);
	end = chrono::system_clock::now();
	cout << ans << '\t' << std::chrono::duration_cast<std::chrono::seconds>(end - start).count()<< '\n';
	*/
	return 0;
}
