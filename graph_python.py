import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import subprocess

limits=list()

loop=list()
rec=list()
form=list()
resp=[loop, form, rec]
fibs=['./fibloop','./fibform','./fibrec']
test_items=range(70)
att=0
for i, fib in enumerate(fibs):
	att=0
	for x in test_items:
		att=x+1
		result = subprocess.run(['timeout', '60', fib], stdout=subprocess.PIPE, input=str(x).encode('utf-8'))
		if result.returncode is not 0:
			att-=1
			break
		linhas = result.stdout.decode('utf-8').strip().split('\n')
		resp[i].append(linhas[0].split('\t')[1])
	limits.append(att)
plt.plot(range(limits[0]), loop)
plt.xlabel('n termo')
plt.ylabel('tempo [ns]')
plt.title("Fibonacci utilizando laços de repetição")
plt.savefig('fibloop.png')
plt.clf()
plt.plot(range(limits[1]), form)
plt.title("Fibonacci utilizando fórmula")
plt.xlabel('n termo')
plt.ylabel('tempo [ms]')
plt.savefig('fibform.png')
plt.clf()
plt.plot(range(limits[2]), rec)
plt.title("Fibonacci utilizando recursividade ingênua")
plt.xlabel('n termo')
plt.ylabel('tempo [s]')
plt.savefig('fibrec.png')

